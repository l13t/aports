# Contributor: Sam Dodrill <shadowh511@gmail.com>
# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: Sam Dodrill <shadowh511@gmail.com>
pkgname=py3-virtualenv
pkgver=20.20.0
pkgrel=0
pkgdesc="Virtual Python3 Environment builder"
options="!check" # Requires unpackaged 'flaky'
url="https://virtualenv.pypa.io/en/latest/"
arch="noarch"
license="MIT"
depends="python3 py3-platformdirs py3-distlib py3-filelock"
makedepends="
	py3-gpep517
	py3-hatchling
	py3-hatch-vcs
	py3-wheel
	"
checkdepends="py3-pytest py3-six"
source="https://files.pythonhosted.org/packages/source/v/virtualenv/virtualenv-$pkgver.tar.gz"
builddir="$srcdir/virtualenv-$pkgver"

replaces="py-virtualenv" # Backwards compatibility
provides="py-virtualenv=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 setup.py test
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
6710efcbead1358479961c84ec3f6b9c3a1fc59c2f6b33fa13e2609554f7072aee7ef8cc07b4aa2fbc27ac950a78e447529fb8ceb2e4ee0881a4356a67d26641  virtualenv-20.20.0.tar.gz
"
