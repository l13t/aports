# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-bigscreen
pkgver=5.27.2
pkgrel=0
pkgdesc="A 10-feet interface made for TVs"
url="https://invent.kde.org/plasma/plasma-bigscreen/"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
license="LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL"
depends="
	kdeconnect
	kirigami2
	plasma-nano
	plasma-nm
	plasma-pa
	plasma-remotecontrollers
	plasma-settings
	plasma-workspace
	"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kactivities-stats-dev
	kcmutils-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami2-dev
	knotifications-dev
	kwayland-dev
	kwindowsystem-dev
	plasma-framework-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	samurai
	"
subpackages="$pkgname-lang"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-bigscreen-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2ff97593a5b00b2df25d4370f98d66fbdd67f6a7126bbbdcacf48f215851f3f3a3a7a5d3ae4e32462c67e96472d366fba8031934dc3d8a4e3087532127ad792a  plasma-bigscreen-5.27.2.tar.xz
"
