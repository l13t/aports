# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdecoration
pkgver=5.27.2
pkgrel=0
pkgdesc="Plugin based library to create window decorations"
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kdecoration-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run -a ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e4083f2661d5768c4ccef4c115650f58d89eb0397cf012a12459ae150f1944e90f08ce22f161fe9a75f7ffc944dd1a37e67833c672eb5f7b60f6119a46cd7b47  kdecoration-5.27.2.tar.xz
"
