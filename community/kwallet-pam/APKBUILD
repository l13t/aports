# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwallet-pam
pkgver=5.27.2
pkgrel=0
pkgdesc="KWallet PAM integration"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later"
depends="socat"
makedepends="
	extra-cmake-modules
	kwallet-dev
	libgcrypt-dev
	linux-pam-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwallet-pam-$pkgver.tar.xz"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/ \
		-DCMAKE_INSTALL_LIBEXECDIR=usr/libexec \
		-DCMAKE_INSTALL_LIBDIR=lib # for the pam module to be in /lib/security
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/lib/systemd
}

sha512sums="
8a4149701251d82f4a2edf76a821e537dd0f0851038d224c9c3c28b2e69e5896588e25f04b58c85e6af5389d02b7bc3b72c961e44255443dabb61b0d96f9ff68  kwallet-pam-5.27.2.tar.xz
"
