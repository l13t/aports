# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze-plymouth
pkgver=5.27.2
pkgrel=0
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/"
pkgdesc="Breeze theme for Plymouth"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	plymouth-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-plymouth-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DDISTRO_NAME="$(grep PRETTY_NAME /etc/os-release | awk -F  = '{ print $2 }')" \
		-DDISTRO_VERSION="$(grep VERSION_ID /etc/os-release | awk -F  = '{ print $2 }')"
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e999adce00ff05a40579314e89edf85790237c9a67cff1d9d36a49628ab03441ce7f77d5bc83033b85d0c161d4849421fff1a8c79aad470086b199ff6c507e14  breeze-plymouth-5.27.2.tar.xz
"
