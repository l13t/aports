# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=flatpak-kcm
pkgver=5.27.2
pkgrel=0
pkgdesc="Flatpak Permissions Management KCM"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	flatpak-dev
	kcmutils-dev
	kconfig-dev
	kdeclarative-dev
	ki18n-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/flatpak-kcm-$pkgver.tar.xz"
subpackages="$pkgname-lang"
install_if="flatpak systemsettings"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# flatpakpermissiontest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "flatpakpermissiontest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5bcae0336af0360e145c38e41a6a06211de36a090d89705e586068b4b04ee9dfac7ccb8ff463f96a112cafe3a837fc1dcebd1e59e63ff9fd00bf850c61804564  flatpak-kcm-5.27.2.tar.xz
"
