# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=vdirsyncer
_pyname=$pkgname
pkgver=0.19.1
pkgrel=0
pkgdesc="CLI Synchronization for CalDAV and CardDAV"
url="http://vdirsyncer.pimutils.org"
arch="noarch"
license="BSD-3-Clause"
depends="
	python3
	py3-aiohttp
	py3-aiostream
	py3-atomicwrites
	py3-click
	py3-click-log
	py3-requests
	py3-requests-toolbelt
	"
makedepends="py3-build py3-installer py3-wheel py3-setuptools_scm py3-sphinx"
checkdepends="
	py3-aioresponses
	py3-pytest
	py3-pytest-asyncio
	py3-pytest-cov
	py3-pytest-httpserver
	py3-hypothesis
	py3-subtesthack
	py3-trustme
	py3-werkzeug
	"
subpackages="$pkgname-doc"
source="https://files.pythonhosted.org/packages/source/v/$_pyname/$_pyname-$pkgver.tar.gz
	"

build() {
	python3 -m build --wheel --skip-dependency-check --no-isolation
	PYTHONPATH="build:$PYTHONPATH" sphinx-build -b man docs/ build/
}

check() {
	# test_request_ssl requires network
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	DETERMINISTIC_TESTS=true testenv/bin/python3 -m pytest --deselect tests/system/utils/test_main.py::test_request_ssl
}

package() {
	python3 -m installer -d "$pkgdir" dist/*.whl
	install -Dm644 build/$pkgname.1 "$pkgdir"/usr/share/man/man1/$pkgname.1
}

sha512sums="
2b0917bf69cfa298600b6b35b50db12ca57e1a963baf2ea9839a3c2f9f686f2f83f47c01e44ad2e12287ecfd470fe1a9ee4a756c0f8985236c2fe8d0454b6a15  vdirsyncer-0.19.1.tar.gz
"
